#!/usr/bin/env bash
rm -rf var/logs/*
rm -rf var/cache/*
chmod 777 -R var/

php bin/console cache:clear --no-warmup --env=prod
php bin/console cache:clear --no-warmup

php bin/console doctrine:schema:update --force
php bin/console doctrine:schema:validate

rm -rf var/logs/*
rm -rf var/cache/*
chmod 777 -R var/

php bin/console assets:install --env=prod --no-debug --symlink
php bin/console assets:install --symlink
php bin/console assetic:dump --env=prod --no-debug
php bin/console assetic:dump


php bin/console cache:warmup
php bin/console cache:warmup --env=prod

chmod 777 -R var/
chmod 777 -R web/

php bin/console mopa:bootstrap:symlink:less
