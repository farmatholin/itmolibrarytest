<?php
/**
 * Created by PhpStorm.
 * User: farmatholin
 * Date: 25.08.17
 * Time: 2:48
 */

namespace AppBundle\Form\Type;


use AppBundle\Entity\Author;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

/**
 * Class BookType
 * @package AppBundle\Form\Type
 */
class BookType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', null, [
            'label' => 'Название книги'
        ])->add('year', null, [
            'label' => 'Год выпуска',
        ])->add('ISBN', null, [
            'label' => 'ISBN'
        ])->add('pages', null, [
            'label' => 'кол-во страниц'
        ]);

        $builder->add('imageFile', FileType::class, [
            'required' => false,
        ]);

        $builder->add('authors', Select2EntityType::class, [
            'multiple' => true,
            'remote_route' => 'authors_select',
            'class' => Author::class,
            'primary_key' => 'id',
            'text_property' => 'fio',
            'minimum_input_length' => 0,
            'page_limit' => 10,
            'allow_clear' => true,
            'placeholder' => 'Выберете Автора',
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'AppBundle\Entity\Book'
            ]
        );
    }

    public function getName()
    {
        return "book_type";
    }
}