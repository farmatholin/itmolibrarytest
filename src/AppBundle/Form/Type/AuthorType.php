<?php
/**
 * Created by PhpStorm.
 * User: farmatholin
 * Date: 25.08.17
 * Time: 1:10
 */

namespace AppBundle\Form\Type;


use AppBundle\Entity\Book;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

/**
 * Class AuthorType
 * @package AppBundle\Form\Type
 */
class AuthorType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('fio', null,[
            'label' => "ФИО",
        ]);

        $builder->add('books', Select2EntityType::class, [
            'multiple' => true,
            'remote_route' => 'books_select',
            'class' => Book::class,
            'primary_key' => 'id',
            'text_property' => 'name',
            'minimum_input_length' => 0,
            'page_limit' => 10,
            'allow_clear' => true,
            'placeholder' => 'Выберете книгу',
        ]);


    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'AppBundle\Entity\Author'
            ]
        );
    }

    public function getName()
    {
        return "author_type";
    }
}