<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Author;
use AppBundle\Form\Type\AuthorType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Template()
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $books = $em->getRepository("AppBundle:Book")->getLimitBooks();

        return [
            'books' => $books,
        ];
    }
}
