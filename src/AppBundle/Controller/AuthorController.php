<?php
/**
 * Created by PhpStorm.
 * User: farmatholin
 * Date: 25.08.17
 * Time: 1:07
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Author;
use AppBundle\Form\Type\AuthorType;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/author")
 *
 * Class AuthorController
 * @package AppBundle\Controller
 */
class AuthorController extends Controller
{

    /**
     * @Route("/", name="author_index")
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()
            ->getManager();

        $authors = $em->getRepository("AppBundle:Author")->getAllAuthors();

        $paginator = $this->get('knp_paginator')->paginate($authors, $request->query->get('page', 1), 20);

        return [
            'paginator' => $paginator,
        ];
    }

    /**
     * @Route("/view/{id}", name="author_view")
     * @Template()
     *
     * @param Request $request
     * @param Author $author
     * @return array
     */
    public function viewAuthorAction(Request $request, Author $author){

        return [
            'author' => $author
        ];
    }

    /**
     * @Route("/new", name="author_new")
     * @Template()
     * @Security(expression="is_granted('IS_AUTHENTICATED_REMEMBERED')")
     *
     * @param Request $request
     * @return array
     */
    public function newAuthorAction(Request $request){

        $author = new Author();

        $form = $this->createForm(AuthorType::class, $author);

        return [
            'author' => $author,
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/create", name="author_create")
     * @Template("@App/Author/newAuthor.html.twig")
     * @Security(expression="is_granted('IS_AUTHENTICATED_REMEMBERED')")
     *
     * @param Request $request
     *
     * @return array|RedirectResponse
     */
    public function createAuthorAction(Request $request){

        $author = new Author();
        $form = $this->createForm(AuthorType::class, $author);

        $form->handleRequest($request);

        if($form->isValid()){
            $em = $this->getDoctrine()
                ->getManager();

            $author->setOwner($this->getUser());

            $em->persist($author);
            $em->flush();

            return $this->redirect($this->generateUrl('author_index'));
        }

        return [
            'author'=>$author,
            'form'=>$form->createView(),
        ];
    }

    /**
     * @Route("/edit/{id}", name="author_edit")
     * @Template()
     * @Security(expression="user.canEditAuthor(author)")
     *
     * @param Request $request
     * @param Author $author
     *
     * @return array
     */
    public function editAuthorAction(Request $request, Author $author){

        $form = $this->createForm(AuthorType::class, $author);

        return[
            'author'=>$author,
            'form'=>$form->createView(),
        ];
    }


    /**
     * @Route("/update/{id}", name="author_update")
     * @Template("@App/Author/editAuthor.html.twig")
     * @Security(expression="user.canEditAuthor(author)")
     *
     * @param Request $request
     * @param Author $author
     * @return array|RedirectResponse
     */
    public function updateAuthorAction(Request $request, Author $author){

        $form = $this->createForm(AuthorType::class, $author);

        $form->handleRequest($request);

        if($form->isValid()){
            $em = $this->getDoctrine()
                ->getManager();

            $em->persist($author);
            $em->flush();

            return $this->redirect($this->generateUrl('author_index'));
        }

        return [
            'author'=>$author,
            'form'=>$form
        ];
    }

    /**
     * @Route("/remove/{id}", name="author_remove")
     * @Security(expression="user.canEditAuthor(author)")
     *
     * @param Request $request
     * @param Author $author
     *
     * @return RedirectResponse
     */
    public function removeAuthorAction(Request $request, Author $author){
        $em = $this->getDoctrine()
            ->getManager();

        $em->remove($author);
        $em->flush();

        return $this->redirect($this->generateUrl("author_index"));
    }

    /**
     * @Route("/_authors_select", name="authors_select")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getAllAuthorsSelectAction(Request $request){

        /** @var EntityManager $em */
        $em = $this->getDoctrine()
            ->getManager();

        $authorsRaw = $em->getRepository("AppBundle:Author")
            ->getAllAuthors();

        $authors = [];

        /** @var Author $author */
        foreach ($authorsRaw as $author){
            $authors[] = $author->getDataForSelect();
        }

        return new JsonResponse([
            'results' => $authors,
            'more' => true,
        ]);
    }
}