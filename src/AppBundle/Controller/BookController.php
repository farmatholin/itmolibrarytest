<?php
/**
 * Created by PhpStorm.
 * User: farmatholin
 * Date: 25.08.17
 * Time: 1:07
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Book;
use AppBundle\Form\Type\BookType;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class BookController
 * @package AppBundle\Controller
 *
 * @Route("/book")
 */
class BookController extends Controller
{

    /**
     * @Route("/", name="book_index")
     * @Template()
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $books = $em->getRepository("AppBundle:Book")->getAllBooks();

        $paginator = $this->get('knp_paginator')->paginate($books, $request->query->get('page', 1), 20);

        return [
            'paginator' => $paginator,
        ];
    }

    /**
     * @Route("/view/{id}", name="book_view")
     * @Template()
     * @param Request $request
     * @param Book $book
     * @return array
     */
    public function viewAction(Request $request, Book $book){

        return [
            'book' => $book
        ];
    }

    /**
     * @Route("/new", name="book_new")
     * @Template()
     * @Security(expression="is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @param Request $request
     * @return array
     */
    public function newBookAction(Request $request) {

        $book = new Book();
        $form = $this->createForm(BookType::class, $book);

        return [
            'form' => $form->createView(),
            'book' => $book,
        ];
    }

    /**
     * @Route("/create", name="book_create")
     * @Method("POST")
     * @Template("AppBundle:Book:newBook.html.twig")
     * @Security(expression="is_granted('IS_AUTHENTICATED_REMEMBERED')")
     *
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function createBookAction(Request $request){
        $book = new Book();
        $form = $this->createForm(BookType::class, $book);

        $form->handleRequest($request);
        if($form->isValid()){

            $em = $this->getDoctrine()->getManager();
            $book->setOwner($this->getUser());

            $em->persist($book);

            $em->flush();

            return $this->redirect($this->generateUrl('book_index'));
        }

        return [
            'form' => $form->createView(),
            'book' => $book,
        ];
    }

    /**
     * @Route("/edit/{id}", name="book_edit")
     * @Template()
     * @Security(expression="user.canEditBook(book)")
     *
     * @param Request $request
     * @param Book $book
     * @return array
     */
    public function editBookAction(Request $request, Book $book) {

        $form = $this->createForm(BookType::class, $book);

        return [
            'form' => $form->createView(),
            'book' => $book,
        ];
    }

    /**
     * @Route("/update/{id}", name="book_update")
     * @Template("@App/Book/editBook.html.twig")
     * @Security(expression="user.canEditBook(book)")
     *
     * @param Request $request
     * @param Book $book
     * @return array|RedirectResponse
     */
    public function updateBookAction(Request $request, Book $book){
        $form = $this->createForm(BookType::class, $book);

        $form->handleRequest($request);
        if($form->isValid()){

            $em = $this->getDoctrine()
                ->getManager();

            // Remove old file if we upload new
            if($book->getImageFile() instanceof UploadedFile){
                $this->get('app.util.file_uploader')
                    ->remove($book->getImagePath());
            }

            $em->persist($book);

            $em->flush();

            return $this->redirect($this->generateUrl('book_index'));
        }

        return [
            'form' => $form->createView(),
            'book' => $book,
        ];
    }

    /**
     * @Route("/remove/{id}", name="book_remove")
     * @Security(expression="user.canEditBook(book)")
     * @param Request $request
     * @param Book $book
     * @return RedirectResponse
     */
    public function removeBookAction(Request $request, Book $book){
        $em = $this->getDoctrine()->getManager();

        $em->remove($book);
        $em->flush();

        return $this->redirect($this->generateUrl('book_index'));

    }

    /**
     * @Route("/_book_select", name="books_select")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getAllBooksSelectAction(Request $request){

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $booksRaw = $em->getRepository("AppBundle:Book")->getAllBooks();

        $books = [];

        /** @var Book $book */
        foreach ($booksRaw as $book){
            $books[] = $book->getDataForSelect();
        }

        return new JsonResponse([
            'results' => $books,
            'more' => true,
        ]);
    }
}