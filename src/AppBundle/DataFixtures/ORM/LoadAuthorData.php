<?php
/**
 * Created by PhpStorm.
 * User: farmatholin
 * Date: 27.08.17
 * Time: 16:37
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Author;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadAuthorData
 * @package AppBundle\DataFixtures\ORM
 */
class LoadAuthorData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /** @var User $user1 */
        $user1 = $this->getReference('user1');
        /** @var User $user2 */
        $user2 = $this->getReference('user2');

        $author1 = new Author();
        $author1->setOwner($user1);
        $author1->setFio("Иванов Иван Иванович");
        $manager->persist($author1);

        $author2 = new Author();
        $author2->setOwner($user1);
        $author2->setFio("Иванов Петр Иванович");

        $manager->persist($author2);

        $author3 = new Author();
        $author3->setOwner($user2);
        $author3->setFio("Сидоров Сергей Александрович");

        $manager->persist($author3);

        $author4 = new Author();
        $author4->setOwner($user2);
        $author4->setFio("Агапова Юлия Сергеевна");

        $manager->persist($author4);


        $manager->flush();

        $this->addReference('author1', $author1);
        $this->addReference('author2', $author2);
        $this->addReference('author3', $author3);
        $this->addReference('author4', $author4);
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 2;
    }
}