<?php
/**
 * Created by PhpStorm.
 * User: farmatholin
 * Date: 27.08.17
 * Time: 16:32
 */

namespace AppBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadUserData
 * @package AppBundle\DataFixtures\ORM
 */
class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $userManager = $this->container->get('fos_user.user_manager');

        // Create our user and set details
        $user1 = $userManager->createUser();
        $user1->setEmail('user1@test.ru');
        $user1->setPlainPassword('test');
        $user1->setEnabled(true);
        $user1->setRoles([
            'ROLE_USER'
        ]);

        // Update the user
        $userManager->updateUser($user1, true);


        $user2 = $userManager->createUser();
        $user2->setEmail('user2@test.ru');
        $user2->setPlainPassword('test');
        $user2->setEnabled(true);
        $user2->setRoles([
            'ROLE_USER'
        ]);

        // Update the user
        $userManager->updateUser($user2, true);

        $this->addReference('user1', $user1);
        $this->addReference('user2', $user2);
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}