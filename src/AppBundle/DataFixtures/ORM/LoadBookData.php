<?php
/**
 * Created by PhpStorm.
 * User: farmatholin
 * Date: 27.08.17
 * Time: 16:50
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Author;
use AppBundle\Entity\Book;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadBookData
 * @package AppBundle\DataFixtures\ORM
 */
class LoadBookData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /** @var User $user1 */
        $user1 = $this->getReference('user1');
        /** @var User $user2 */
        $user2 = $this->getReference('user2');

        /** @var Author $author1 */
        $author1 = $this->getReference('author1');
        /** @var Author $author2 */
        $author2 = $this->getReference('author2');
        /** @var Author $author3 */
        $author3 = $this->getReference('author3');
        /** @var Author $author4 */
        $author4 = $this->getReference('author4');

        $book1 = new Book();

        $book1->setOwner($user1)
            ->setName("Первый после Иванова")
            ->setISBN("2-266-11156-7")
            ->setYear("2016")
            ->setPages("2513")
            ->addAuthor($author1)
            ->addAuthor($author2)
            ;

        $manager->persist($book1);

        $book2 = new Book();

        $book2->setOwner($user1)
            ->setName("Почему мы Ивановы")
            ->setISBN("2-266-11156-6")
            ->setYear("2015")
            ->setPages("2500")
            ->addAuthor($author1)
            ->addAuthor($author2)
        ;

        $manager->persist($book2);

        $book3 = new Book();

        $book3->setOwner($user2)
            ->setName("Почему я не Иванов")
            ->setISBN("2-266-11156-8")
            ->setYear("2017")
            ->setPages("3700")
            ->addAuthor($author3)
        ;

        $manager->persist($book3);

        $book4 = new Book();

        $book4->setOwner($user2)
            ->setName("Приключения Юли том. 1")
            ->setISBN("2-266-11156-9")
            ->setYear("2015")
            ->setPages("500")
            ->addAuthor($author4)
        ;

        $manager->persist($book4);

        $book5 = new Book();

        $book5->setOwner($user2)
            ->setName("Приключения Юли том. 2")
            ->setISBN("2-266-11157-0")
            ->setYear("2017")
            ->setPages("1500")
            ->addAuthor($author4)
        ;

        $manager->persist($book5);


        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 3;
    }
}