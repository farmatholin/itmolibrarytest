<?php
/**
 * Created by PhpStorm.
 * User: farmatholin
 * Date: 24.08.17
 * Time: 15:34
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Author
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\AuthorRepository")
 * @ORM\Table(name="author")
 *
 * @UniqueEntity(
 *     fields={"fio"},
 *     message="Такой автор уже есть"
 * )
 */
class Author
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    protected $fio;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Book", inversedBy="authors")
     * @ORM\JoinTable(name="books_authors")
     */
    protected $books;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="authors")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $owner;

    public function __construct()
    {
        $this->books = new ArrayCollection();
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     * @return $this
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFio()
    {
        return $this->fio;
    }

    /**
     * @param mixed $fio
     * @return $this
     */
    public function setFio($fio)
    {
        $this->fio = $fio;
        return $this;
    }

    /**
     * @return string
     */
    public function getFioFormatted(){
        $fioRaw = explode(" ", $this->fio);
        $fioFormatted = $fioRaw[0];
        for($i = 1; $i < count($fioRaw); ++$i){
            $fioFormatted .= " ".mb_substr($fioRaw[$i], 0, 1).".";

        }
        return $fioFormatted;
    }

    /**
     * @param Book $book
     * @return $this
     */
    public function addBook(Book $book)
    {
        $this->books->add($book);
        return $this;
    }

    /**
     * @param Book $book
     * @return $this
     */
    public function removeBook(Book $book)
    {
        $this->books->removeElement($book);
        return $this;
    }

    /**
     * Get rooms
     *
     * @return \Doctrine\Common\Collections\Collection | Book[]
     */
    public function getBooks()
    {
        return $this->books;
    }

    public function getDataForSelect()
    {
        return [
            'id' => $this->getId(),
            'text' => $this->getFio()
        ];
    }
}