<?php
/**
 * Created by PhpStorm.
 * User: farmatholin
 * Date: 24.08.17
 * Time: 15:18
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Book
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\BookRepository")
 * @ORM\Table(name="book")
 *
 * @UniqueEntity(
 *     fields={"ISBN"},
 *     message="Такая книга уже есть"
 * )
 *
 * @UniqueEntity(
 *     fields={"name", "year"},
 *     message="Такая книга уже есть",
 *     errorPath="name",
 * )
 */
class Book
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $year;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    protected $ISBN;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $pages;


    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="books")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $owner;
    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Author", mappedBy="books", cascade={"persist", "remove"})
     */
    protected $authors;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    protected $imagePath;

    /**
     * @var UploadedFile
     *
     * @Assert\File(
     *     mimeTypes={"image/jpeg", "image/pjpeg", "image/png", "image/x-png"}
     * )
     */
    protected $imageFile;


    public function __construct()
    {
        $this->authors = new ArrayCollection();
    }

    /**HeadooUserBundle:User
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     * @return $this
     */
    public function setYear($year)
    {
        $this->year = $year;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getISBN()
    {
        return $this->ISBN;
    }

    /**
     * @param mixed $ISBN
     * @return $this
     */
    public function setISBN($ISBN)
    {
        $this->ISBN = $ISBN;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * @param mixed $pages
     * @return $this
     */
    public function setPages($pages)
    {
        $this->pages = $pages;
        return $this;
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     * @return $this
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * @param string $imagePath
     * @return $this
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;
        return $this;
    }


    /**
     * @param Author $author
     * @return $this
     */
    public function addAuthor(Author $author)
    {
        $this->authors->add($author);
        $author->addBook($this);

        return $this;
    }

    /**
     * @param Author $author
     * @return $this
     */
    public function removeAuthor(Author $author)
    {
        $this->authors->removeElement($author);
        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param UploadedFile $imageFile
     */
    public function setImageFile($imageFile)
    {
        $this->imageFile = $imageFile;
    }

    /**
     * Get rooms
     *
     * @return \Doctrine\Common\Collections\Collection | Author[]
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    public function getDataForSelect()
    {
        return [
            'id' => $this->getId(),
            'text' => $this->getName()
        ];
    }

}