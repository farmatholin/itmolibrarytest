<?php

/**
 * Created by PhpStorm.
 * User: farmatholin
 * Date: 24.08.17
 * Time: 14:30
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class User
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\UserRepository")
 * @ORM\Table(name="users")
 *
 * @UniqueEntity(
 *     fields={"usernameCanonical"},
 *     errorPath="username",
 *     message="fos_user.username.already_used"
 * )
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     */
    protected $email;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Author", mappedBy="owner", cascade={"persist", "remove"})
     */
    private $authors;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Book", mappedBy="owner", cascade={"persist", "remove"})
     */
    private $books;

    public function __construct()
    {
        parent::__construct();
        // your own logic

        $this->authors = new ArrayCollection();
        $this->books = new ArrayCollection();
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        parent::setUsername($email);
        parent::setEmail($email);

        return $this;
    }

    /**
     * @param Author $author
     * @return $this
     */
    public function addAuthor(Author $author)
    {
        $this->authors->add($author);
        return $this;
    }

    /**
     * @param Author $author
     * @return $this
     */
    public function removeAuthor(Author $author)
    {
        $this->authors->removeElement($author);
        return $this;
    }

    /**
     * Get rooms
     *
     * @return \Doctrine\Common\Collections\Collection | Author[]
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * @param Book $book
     * @return $this
     */
    public function addBook(Book $book)
    {
        $this->books->add($book);
        return $this;
    }

    /**
     * @param Book $book
     * @return $this
     */
    public function removeBook(Book $book)
    {
        $this->books->removeElement($book);
        return $this;
    }

    /**
     * Get rooms
     *
     * @return \Doctrine\Common\Collections\Collection | Book[]
     */
    public function getBooks()
    {
        return $this->books;
    }

    /**
     * @param Author $author
     * @return bool
     */
    public function canEditAuthor(Author $author){
        if($this === $author->getOwner()){
            return true;
        }
        return false;
    }

    /**
     * @param Book $book
     * @return bool
     */
    public function canEditBook(Book $book){
        if($this === $book->getOwner()){
            return true;
        }
        return false;
    }
}