<?php
/**
 * Created by PhpStorm.
 * User: farmatholin
 * Date: 25.08.17
 * Time: 17:21
 */

namespace AppBundle\EventListener;


use AppBundle\Entity\Book;
use AppBundle\Util\FileUploader;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploadEventListener implements EventSubscriber
{
    private $uploader;

    public function __construct(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'prePersist',
            'preUpdate',
            'preRemove'
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $this->upload($entity);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $this->upload($entity);

    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Book) {
            $this->uploader->remove($entity->getImagePath());
        }
    }

    /**
     * @param $entity
     */
    private function upload($entity){
        if ($entity instanceof Book) {
            $file = $entity->getImageFile();
            if($file instanceof UploadedFile){
                $filePath = $this->uploader->upload($file);
                $entity->setImagePath($filePath);
            }
        }
    }
}