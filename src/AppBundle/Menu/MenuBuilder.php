<?php
/**
 * Created by PhpStorm.
 * User: farmatholin
 * Date: 24.08.17
 * Time: 23:51
 */

namespace AppBundle\Menu;


use AppBundle\Entity\User;
use Knp\Menu\FactoryInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class MenuBuilder
{
    private $factory;
    protected $tokenStorage;
    protected $authorizationChecker;

    public function __construct(
        FactoryInterface $factory,
        TokenStorageInterface $tokenStorage,
        AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->factory = $factory;
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function createMainMenu() {
        $menu = $this->factory->createItem('root', array(
            'navbar' => true,
        ));

        $menu->addChild('Главная', [
            'route' => 'homepage'
        ]);

        $menu->addChild('Книги', [
            'route' => 'book_index'
        ]);

        $menu->addChild('Авторы', [
            'route' => 'author_index'
        ]);

        return $menu;
    }

    public function createRightSideMenu()
    {
        $menu = $this->factory->createItem('root', [
            'navbar' => true,
            'push_right' => true,
        ]);

        //$user = $this->tokenStorage->getToken()->getUser();

        $menu->setChildrenAttribute('class','links-top list-inline');

        if ($this->authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $menu->addChild('Выход', [
                'route' => 'fos_user_security_logout'
            ]);
        } else {
            $menu->addChild('Регистрация', [
                'route' => 'fos_user_registration_register'
            ]);
            $menu->addChild('Войти', [
                'route' => 'fos_user_security_login'
            ]);
        }

        return $menu;
    }
}