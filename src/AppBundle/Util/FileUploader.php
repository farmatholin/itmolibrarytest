<?php

/**
 * Created by PhpStorm.
 * User: farmatholin
 * Date: 25.08.17
 * Time: 16:58
 */
namespace AppBundle\Util;


use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * This class is created for upload files.
 * Gedmo doctrine extension Uploadable doesn`t work
 * Class FileUploader
 * @package AppBundle\Util
 *
 */
class FileUploader
{
    /**
     * @var string
     */
    private $targetDir;

    /**
     * @var Filesystem
     */
    private $fs;

    /**
     * FileUploader constructor.
     * @param $path
     */
    public function __construct($path)
    {
        $this->targetDir = $path;
        $this->fs = new Filesystem();
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function upload(UploadedFile $file)
    {
        $fileName = md5(uniqid()).'.'.$file->guessExtension();

        $file->move($this->getTargetDir(), $fileName);

        return $fileName;
    }

    /**
     * @param $fileName
     */
    public function remove($fileName) {
        if($this->fs->exists($this->getTargetDir() . "/" . $fileName)){
            $this->fs->remove($this->getTargetDir() . "/" . $fileName);
        }
    }

    /**
     * @return string
     */
    public function getTargetDir()
    {
        return $this->targetDir;
    }

}